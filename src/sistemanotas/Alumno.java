/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemanotas;

import java.util.Scanner;

/**
 *
 * @author Duoc
 */
public class Alumno {

    private String nombre;
    private String rut;
    private Asignatura[] asignaturas;
    private int totalAsignaturas;
    private int asignaturasTomadas;

    public Alumno(String nombre, String rut, int totalAsignaturas) {
        this.nombre = nombre;
        this.rut = rut;
        setTotalAsignaturas(totalAsignaturas);
        asignaturas = new Asignatura[this.totalAsignaturas];
        asignaturasTomadas = 0;
    }

    public void agregarAsignatura(Asignatura asignatura) {
        if (asignaturasTomadas < this.totalAsignaturas) {
            asignaturas[asignaturasTomadas] = asignatura;
            asignaturasTomadas++;
            System.out.println("Asignatura agregada");
        }else{
            System.out.println("No puede agregar mas asignaturas");
        }
    }

    public void imprimePersonaAsignaturas() {
        imprimePersona();
        for (Asignatura aux : asignaturas) {
            if (aux != null) {
                aux.imprimeAsignatura();
            } else {
                break;
            }
        }
    }

    public void imprimePersona() {
        System.out.println("Nombre: " + nombre);
        System.out.println("Rut: " + nombre);
    }

    public int getTotalAsignaturas() {
        return totalAsignaturas;
    }

    private void setTotalAsignaturas(int totalAsignaturas) {
        if (totalAsignaturas > 2 && totalAsignaturas < 6) {
            this.totalAsignaturas = totalAsignaturas;
        } else {
            System.out.println("Debe ser mayor o igual a 3 y menor o igual a 6");
        }
    }
    /*
    public static Alumno newAlumnoScanner(){
         Scanner sc = new Scanner(System.in);
        
        String nombre, rut = "";
        int cantidaAsignaturas;
        
        System.out.print("Ingrese Rut: ");
        rut = sc.nextLine();
        
        System.out.print("Ingrese Nombre: ");
        nombre = sc.nextLine();
        
        System.out.print("Ingrese Cantidad Asignaturas: ");
        cantidaAsignaturas = Integer.parseInt(sc.nextLine());
        
        Alumno a = new Alumno(rut, nombre, cantidaAsignaturas);
        return a;
    }*/
}
