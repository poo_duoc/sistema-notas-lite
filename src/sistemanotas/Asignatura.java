/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemanotas;

/**
 *
 * @author Duoc
 */
public class Asignatura {
    
    private String nombre;
    private double [] notas;
    private double examen;
    private boolean isAprueba;
    private double promedioFinal;
    private int cantidadNotas;
    private int indexNotas;

    public Asignatura(String nombre, int cantidadNotas) {
        this.nombre = nombre;
        this.cantidadNotas = cantidadNotas;
        this.indexNotas = 0;
        this.notas = new double[cantidadNotas];
    }

    public void setExamen(double examen) {
        this.examen = examen;
    }
    
    public void agregarNota(double nota){
        if(indexNotas < this.cantidadNotas) {
            notas[indexNotas] = nota;
            indexNotas++;
        }else{
            
        }
    }
    
    public double getPromedioFinal(){
        double sumaNotas = 0;
        for(double notasAux : notas){
            sumaNotas += notasAux;
        }
        promedioFinal = ((sumaNotas/cantidadNotas)*0.6) + (examen * 0.4);
        return promedioFinal;
    }
    
    public void imprimeAsignatura(){
        int nroNota = 1;
        for(double notaAux: notas){            
            System.out.println("Nota " + nroNota++ +  ": " + notaAux);
        }
        System.out.println("Nota Examen: " + examen);
        System.out.println("Promedio Final: " + getPromedioFinal());
    } 
    
    
}
