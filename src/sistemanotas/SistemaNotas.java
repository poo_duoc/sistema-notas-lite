/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemanotas;

import java.util.Scanner;

/**
 *
 * @author Duoc
 */
public class SistemaNotas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String nombre, rut = "";
        int cantidaAsignaturas;

        System.out.print("Ingrese Rut: ");
        rut = sc.nextLine();

        System.out.print("Ingrese Nombre: ");
        nombre = sc.nextLine();

        System.out.print("Ingrese Cantidad Asignaturas: ");
        cantidaAsignaturas = Integer.parseInt(sc.nextLine());

        Alumno alumno = new Alumno("1-9", "prueba", cantidaAsignaturas);//rut, nombre, cantidaAsignaturas);

        for (int y = 0; y < cantidaAsignaturas; y++) {
            String nombreAsignatura;
            int cantidaNotas;

            System.out.println("Ingrese nombre asignatura: ");
            nombreAsignatura = sc.nextLine();
            System.out.println("Ingrese Cantidad de Notas: ");
            cantidaNotas = Integer.parseInt(sc.nextLine());
            Asignatura poo2201 = new Asignatura(nombreAsignatura, cantidaNotas);

            for (int x = 0; x < cantidaNotas; x++) {
                double notaAux;
                System.out.print("Ingrese Nota: ");
                notaAux = Double.parseDouble(sc.nextLine());
                poo2201.agregarNota(notaAux);
            }

            double examen;
            System.out.print("Ingrese Nota examen: ");
            examen = Double.parseDouble(sc.nextLine());
            poo2201.setExamen(examen);
            alumno.agregarAsignatura(poo2201);
        }
        /* Asignatura poo3501 = new Asignatura("Poo3501", 3);
         Asignatura mdd0101 = new Asignatura("MDD0101", 3);
         a.agregarAsignatura(poo2201);
         a.agregarAsignatura(poo3501);
         a.agregarAsignatura(mdd0101);*/

        alumno.imprimePersonaAsignaturas();

    }

}
